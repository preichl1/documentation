import argparse
import importlib
import io
import itertools
import re
import sys

def eprint(*args, **kwargs):
    kwargs['file'] = sys.stderr
    print(*args, **kwargs)

def import_modules(*args):
    result = []
    missing = []
    for name in args:
        pip_name = name
        if isinstance(name, tuple):
            name, pip_name = name
        try:
            result.append(importlib.import_module(name))
        except ModuleNotFoundError:
            missing.append(pip_name)
    if missing:
        eprint('ERROR: missing Python modules. Please run one of the following:')
        names = ' '.join('python3-' + name for name in missing)
        eprint('  (1) dnf install {}'.format(names))
        for i, name in enumerate(missing):
            eprint('  {} pip install {}'.format('(2)' if i == 0 else '   ', name))
        sys.exit(1)
    return result

jinja2, jsonschema, yaml = import_modules('jinja2', 'jsonschema', ('yaml', 'pyyaml'))


class CommandError(Exception):
    pass


class BaseCommand:
    overview = ''

    def __init__(self, subparsers):
        self.name = type(self).__name__.lower()
        if self.name.startswith('command'):
            self.name = self.name[7:]
        self.parser = subparsers.add_parser(self.name, help=self.overview)
        self.parser.set_defaults(obj=self)
        self.add_arguments()

    def add_arguments(self):
        pass

    def handle(self, args):
        pass

    def add_argument_owners(self):
        self.parser.add_argument('owners', type=self.load_file,
                                 help='path to owners.yaml')

    def add_argument_owners_schema(self):
        self.parser.add_argument('owners_schema', type=self.load_file,
                                 help='path to owners-schema.yaml')

    def load_file(self, path):
        try:
            with open(path) as f:
                return (path, f.read())
        except IOError as e:
            raise argparse.ArgumentTypeError(str(e))

    def load_yaml(self, path, data):
        try:
            stream = io.StringIO(data)
            stream.name = path
            return yaml.safe_load(stream)
        except yaml.YAMLError as e:
            raise CommandError(str(e))


class CommandVerify(BaseCommand):
    overview = 'Verify owners.yaml for correctness.'

    def add_arguments(self):
        self.add_argument_owners()
        self.add_argument_owners_schema()

    def _check_schema(self, args):
        owners = self.load_yaml(*args.owners)
        owners_schema = self.load_yaml(*args.owners_schema)
        try:
            format_checker = jsonschema.Draft202012Validator.FORMAT_CHECKER
        except AttributeError:
            # An older jsonschema package. Use something that works down to RHEL 8.
            format_checker = jsonschema.draft4_format_checker
        try:
            jsonschema.validate(owners, owners_schema, format_checker=format_checker)
        except jsonschema.exceptions.ValidationError as e:
            msg = e.message
            if e.validator == 'type' and e.instance is None:
                msg = 'Value cannot be empty. Either specify the value or omit the key "{}" completely.'.format(e.path[-1])
            try:
                msg = e.schema['message'][e.validator]
            except KeyError:
                pass
            loc = ' -> '.join(map(str, e.absolute_path))
            try:
                if e.absolute_path[0] == 'subsystems':
                    subsys = owners['subsystems'][e.absolute_path[1]]['subsystem']
                    new_loc = ['subsystem "{}"'.format(subsys)]
                    new_loc.extend(list(e.absolute_path)[2:])
                    loc = ' -> '.join(map(str, new_loc))
            except (IndexError, KeyError):
                pass

            oname = args.owners[0]
            if loc:
                raise CommandError('ERROR: {}: {}: {}'.format(oname, loc, msg))
            else:
                raise CommandError('ERROR: {}: {}'.format(oname, msg))

    def _check_more(self, args):
        # We need to check that users are referenced only via aliases. However, information
        # about anchors and aliases is not present in the parsed tree. The nodes created via
        # aliases reference the anchored object in the parsed tree; we could check that all
        # objects in the maintainers/reviewers list are present also in the users list. But
        # this does not provide the direction (one could have an anchor in the subsystem and
        # alias it from users) and, more importantly, does not work with overrides. Instead,
        # replace all anchors and aliases by a custom key. This is somewhat hacky but works
        # well for the current format of owners.yaml.
        oname = args.owners[0]
        data = re.sub(r'^( *- )(<<: *)?\*', r'\1_alias: ', args.owners[1], flags=re.M)
        data = re.sub(r'^( *- )&', r'\1_anchor: ', data, flags=re.M)
        owners = self.load_yaml(oname, data)

        errors = []

        # build users dict
        class User:
            def __init__(self, data):
                self.data = data
                self.used = False

        users = {}
        for data in owners['users']:
            if '_anchor' not in data:
                errors.append('ERROR: {}: user "{}" does not have a YAML anchor.'.format(oname, data.get('name', data)))
                continue
            users[data['_anchor']] = User(data)

        subsys_names = set()
        for subsys in owners['subsystems']:
            subsys_name = subsys['subsystem'].upper()
            if subsys_name in subsys_names:
                errors.append('ERROR: {}: subsystem "{}" is specified multiple times.'.format(oname, subsys_name))
            subsys_names.add(subsys_name)

            # Check that maintainers and reviewers are specified as aliases.
            for person in itertools.chain(subsys['maintainers'], subsys.get('reviewers', ())):
                if '_alias' in person:
                    user = users[person['_alias']]
                    user.used = True
                    # can override only the "restricted" field
                    if any(k not in ('_alias', 'restricted') for k in person):
                        errors.append('ERROR: {}: subsystem "{}": '.format(oname, subsys['subsystem']) +
                                      'Can only override the "restricted" field for the maintainer/reviewer entry "{}"'.format(user.data['name']))
                else:
                    errors.append('ERROR: {}: subsystem "{}": '.format(oname, subsys['subsystem']) +
                                  'The maintainer/reviewer entry for "{}" must be added to the "users" section and be only referenced in the subsystem.'.format(person.get('name', person)))

            # The checks below do not apply to the kernel maintainer entries.
            if subsys['labels']['name'] in ('redhat', 'fedora'):
                continue

            # Check for duplicates between maintainers and reviewers.
            seen = set()
            for person in itertools.chain(subsys['maintainers'], subsys.get('reviewers', ())):
                if '_alias' not in person:
                    continue
                if person['_alias'] in seen:
                    user = users[person['_alias']]
                    errors.append('ERROR: {}: subsystem "{}": '.format(oname, subsys['subsystem']) +
                                  'Duplicate maintainer/reviewer entry for "{}".'.format(user.data['name']))
                seen.add(person['_alias'])

            # requiredApproval can be true only if there are at least
            # 3 maintainers/reviewers.
            if subsys.get('requiredApproval') and len(seen) < 3:
                errors.append('ERROR: {}: subsystem "{}": '.format(oname, subsys['subsystem']) +
                              'requiredApproval can be set only if there are at least 3 maintainers and/or reviewers.')

        # Check that there are no unused users. Such user entries would become
        # unmaintained over time and would silently accumulate. Also, verify that
        # a single person is not specified multiple times in the users section.
        seen = set()
        for user in users.values():
            bare = (user.data['email'], user.data['gluser'])
            if bare in seen:
                errors.append('ERROR: {}: user "{}": '.format(oname, user.data['name']) +
                              'Duplicate user entry.')
            seen.add(bare)
            if not user.used:
                errors.append('ERROR: {}: user "{}": '.format(oname, user.data['name']) +
                              'Not referenced from any subsystem as a maintainer or a reviewer.')

        if errors:
            raise CommandError('\n'.join(errors))

    def handle(self, args):
        self._check_schema(args)
        self._check_more(args)


class CommandDoc(BaseCommand):
    overview = 'Generate documentation from owners.yaml schema.'

    def add_arguments(self):
        self.add_argument_owners_schema()

    def format_footnote(self, content):
        return f' footnote:[{content}]' if content else ''

    def format_description(self, content, optional=False):
        data = content.get('description', '').split('REQUIRES-', 1)
        description = data[0].strip()
        rewrites = None
        if len(data) > 1:
            rewrites = [s.strip() for s in data[1].split(':', 1)]
        if optional:
            description = '{}{}Optional.'.format(description, ' ' if description else '')
        if 'enum' in content:
            description = '{}{}Allowed values: {}.'.format(
                            description,
                            ' ' if description else '',
                            ', '.join(f'`{c}`' for c in content['enum']))
        return self.format_footnote(description), rewrites

    def format_properties(self, obj, depth=0, is_array=False, rewrites=None):
        result = []
        required = obj.get('required', ())
        items = obj['properties'].items()
        if rewrites:
            description = self.format_footnote(rewrites[1])
            if rewrites[0] == 'ALIASES':
                result.append(f'*__link__...{description}')
                items = ()  # do not iterate through child items
            elif rewrites[0] == 'ANCHORS':
                result.append(f'&__link__{description}')
        for prop, content in items:
            description, subrewrites = self.format_description(content,
                                                               optional=prop not in required)
            prop_type = content['type']
            if prop_type == 'string':
                result.append(f'{prop}: __value__{description}')
            elif prop_type == 'boolean':
                result.append(f'{prop}: __true / false__{description}')
            elif prop_type == 'object':
                result.append(f'{prop}:{description}')
                result.extend(self.format_properties(content, depth + 1))
            elif prop_type == 'array':
                result.append(f'{prop}:{description}')
                item_type = content['items']['type']
                if item_type == 'string':
                    result.append('  - __value...__' +
                                  self.format_description(content['items'])[0])
                elif item_type == 'object':
                    result.extend(self.format_properties(content['items'], depth + 1,
                                                         is_array=True,
                                                         rewrites=subrewrites))
                else:
                    eprint(f'Usupported array type: {item_type}')
                    sys.exit(1)
            else:
                eprint(f'Unsupported property type: {prop_type}')
                sys.exit(1)
        fmt_result = []
        indent = '  ' * depth
        for line in result:
            if is_array:
                fmt_result.append(f'{indent}- {line}')
                indent += '  '
                is_array = False
            else:
                fmt_result.append(indent + line)
        return fmt_result

    def handle(self, args):
        print('''---
title: The Format of owners.yaml
weight: 100
---

[subs="+quotes,+macros"]
----''')
        print('\n'.join(self.format_properties(self.load_yaml(*args.owners_schema))))
        print('----')


class ChainableUndefined(jinja2.Undefined):
    def __getattr__(self, name):
        return self


class CommandConvert(BaseCommand):
    overview = 'Convert owners.yaml using a template.'

    def add_arguments(self):
        self.add_argument_owners()
        self.parser.add_argument('template',
                                 help='path to the template')

    def handle(self, args):
        env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'),
                                 undefined=ChainableUndefined,
                                 trim_blocks=True)
        try:
            template = env.get_template(args.template)
        except jinja2.TemplateNotFound as e:
            raise CommandError('ERROR: template not found: {}'.format(e))
        print(template.render(self.load_yaml(*args.owners)), end='')


class CommandJson(BaseCommand):
    overview = 'Export owners.yaml to JSON.'

    def add_arguments(self):
        self.add_argument_owners()

    def handle(self, args):
        import json
        print(json.dumps(self.load_yaml(*args.owners), indent=4))
        return True


parser = argparse.ArgumentParser(description='Verify and convert owners.yaml.')
subparsers = parser.add_subparsers(dest='command', title='available commands',
                                   metavar='COMMAND')
for name, cls in list(globals().items()):
    if name.startswith('Command') and issubclass(cls, BaseCommand):
        cls(subparsers)
args = parser.parse_args()
if not args.command:
    parser.print_help()
    sys.exit(1)
try:
    args.obj.handle(args)
except CommandError as e:
    eprint(e)
    sys.exit(1)
